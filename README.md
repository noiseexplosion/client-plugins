Welcome to 2009scape's official client plugin repository! This serves as a method to distribute all of our plugins that we created for use with 2009scape. 

# Installation Instructions
## Step 1: Download This Repo
![chrome_XR5EaMHHjT](/uploads/a5392f50cb321e1f476ad43045ad3bef/chrome_XR5EaMHHjT.gif)

## Step 2: Locate the 2009scape Client Files Folder.

**NOTE: On Linux, this is just ~/.local/share/2009scape**

1. Press the Windows key and R at the same time, to open the run dialog. 
2. Type %HOMEPATH%
3. Press Ok
4. Locate and enter the 2009scape folder

This will open your HOME directory, which is where we store the files on Windows.

## Steps 3 and 4: Create a new folder called plugins, and copy the plugins you want from the .zip into the folder

We recommend XP drops, slayer tracker, and input QOL for all users. There are additional debug plugins if you desire them.

![explorer_E9EtGHCO3Z](/uploads/fa6c9fe1201d8bf19b88d703b45ad56f/explorer_E9EtGHCO3Z.gif)

## Step 5: Restart the client (using the new experimental client) and you're done!

It's just that easy.
